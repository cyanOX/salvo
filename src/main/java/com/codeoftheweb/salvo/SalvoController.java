package com.codeoftheweb.salvo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
    public class SalvoController {

    public PasswordEncoder passwordEncoder(){
        return (PasswordEncoder) PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Autowired
    private SalvoRepository salvoRepository;

    @Autowired
    private ShipRepository shipRepository;

    @Autowired
    private GamePlayerRepository gamePlayerRepository;

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private PlayerRepository playerRepository;

    //@RequestMapping("/games")
    public List<Object> getGames(){
        return gameRepository
                .findAll()
                .stream()
                .map(game -> gameDto(game))
                .collect(Collectors.toList());
    }

    public Map<String , Object> gameDto(Game game){
        Map<String, Object>dto = new LinkedHashMap<>();
        dto.put("id",game.getId());
        dto.put("creationDate",game.getCreationDate());
        dto.put("gamePlayers",getGamePlayerLista(game.getGamePlayers()));
        dto.put("scores", getScoresLista(game.getScore()));
        return dto;
    }

    public List<Map<String,Object>> getGamePlayerLista(Set<GamePlayer> gamesPlayers) {
            return gamesPlayers
                    .stream()
                    .map(gamePlayer -> gamePlayerDTO(gamePlayer))
                    .collect(Collectors.toList());
        }

    public Map<String , Object> gamePlayerDTO(GamePlayer gamePlayer){
        Map<String, Object>dto = new LinkedHashMap<>();
        dto.put("id",gamePlayer.getId());
        dto.put("player",playerDTO(gamePlayer.getPlayer()));
        return dto;
    }
    public Map<String , Object> playerDTO(Player player){
        Map<String, Object>dto = new LinkedHashMap<>();
        dto.put("id",player.getId());
        dto.put("username", player.getUserName());
        dto.put("scores", makeScoresList(player));
        dto.put("gpid", player.getId());
        return dto;
    }

    @RequestMapping ("/game_view/{id}")
    public Map<String,Object> getGameView(@PathVariable long id) {
        return gameViewDTO(gamePlayerRepository.findById(id).get());
    }


    public Map<String, Object> gameViewDTO(GamePlayer gamePlayer){
        Map<String, Object>dto = new LinkedHashMap<>();
        List<GamePlayer> gamePlayers = gamePlayerRepository.findAll().stream().filter( gp -> gp.getGame().getId() == gamePlayer.getGame().getId()).collect(Collectors.toList());
        GamePlayer self = gamePlayers.stream().filter(gp -> gp.getId() == gamePlayer.getId()).findFirst().orElse(null);
        GamePlayer opponent = gamePlayers.stream().filter(gp -> gp.getId() != gamePlayer.getId()).findFirst().orElse(null);


        dto.put("id", gamePlayer.getId());
        dto.put("creationDate", gamePlayer.getJoinDate());
        dto.put("gamePlayers", getGamePlayerLista(gamePlayer.getGame().getGamePlayers()));
        dto.put("ships", getShipLista(gamePlayer.getShips()));
        dto.put("salvoes", makeSalvoList (gamePlayer.getGame()));
        dto.put("hits",makeHitsDTO(self , opponent));
        return dto;

    }

    public List<Map<String,Object>> getShipLista(Set<Ship> ships){
        return ships
                .stream()
                .map(Ship -> shipDTO(Ship))
                .collect(Collectors.toList());
    }

    public Map<String , Object> shipDTO(Ship ship){
        Map<String, Object>dto = new LinkedHashMap<>();
        dto.put("id",ship.getId());
        dto.put("type", ship.getShipType());
        dto.put("locations",ship.getLocations());

        return dto;
    }

    public List<Map<String,Object>> makeSalvoList(Game game){
        List <Map<String, Object>> myList = new ArrayList<>();
        game.getGamePlayers().forEach(gamePlayer -> myList.addAll(getSalvoList(gamePlayer.getSalvoes())));
        return myList;
    }

    public List<Map<String,Object>> getSalvoList(Set<Salvo> salvoes){
        return salvoes
                .stream()
                .map(Salvo -> salvoDTO(Salvo))
                .collect(Collectors.toList());
    }
    public Map<String , Object> salvoDTO(Salvo salvo){
        Map<String, Object>dto = new LinkedHashMap<>();
        dto.put("turno",salvo.getTurno());
        dto.put("player", salvo.getGamePlayer().getPlayer());
        dto.put("locations",salvo.getLocations());

        return dto;
    }

    public Map<String , Object> ScoreDTO(Score score){
        Map<String, Object>dto = new LinkedHashMap<>();
        dto.put("playerId", score.getPlayer().getId());
        dto.put("score", score.getScore());
        dto.put("finishDate", score.getFinishDate());

        return dto;
    }

    public Map<String,Object> makeScoresList(Player player){
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("name", player.getUserName());
        dto.put("total", player.getScore(player));
        dto.put("won", player.getWins(player.getScores()));
        dto.put("lost", player.getLoses(player.getScores()));
        dto.put("tied", player.getDraws(player.getScores()));
        return dto;
    }

    private List<Map<String, Object>>getScoresLista(Set<Score>scores){
        return scores
                .stream()
                .map(score -> ScoreDTO(score))
                .collect(Collectors.toList());
    }

    Player getLoggedPlayer(Authentication authentication) {
        return playerRepository.findByUserName(authentication.getName());
    }

    @RequestMapping("/leaderBoard")
    public List<Map<String,Object>> makeLeaderBoard() {
        return playerRepository
                .findAll()
                .stream()
                .map(player -> playerDTO(player))
                .collect(Collectors.toList());
    }
    @RequestMapping(path = "/game/{id}/players", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> joinGame (Authentication authentication , @PathVariable long id){
        if(authentication == null || authentication instanceof AnonymousAuthenticationToken) {
            return new ResponseEntity<>(makeMap("error", "Unauthorized"), HttpStatus.UNAUTHORIZED);
        }
        Player currentPlayer = playerRepository.findByUserName(authentication.getName());

        Game game = gameRepository.findById(id).get();
        if ( game == null) {
            return new ResponseEntity<>(makeMap("error", "No such game"), HttpStatus.FORBIDDEN);
        }

        long playersCount = gameRepository.findAll().stream().filter(g -> g.getId() == id).count();
        if (playersCount == 2) {
            return new ResponseEntity<>(makeMap("error", "Game is full"), HttpStatus.FORBIDDEN);
        }
        GamePlayer addedGamePlayer = new GamePlayer(game , currentPlayer );
        long savedGamePlayerId = gamePlayerRepository.save(addedGamePlayer).getId();

        return new ResponseEntity<>(makeMap("gpid", savedGamePlayerId), HttpStatus.CREATED);
    }
    @RequestMapping("/games")
    public Map<String, Object> makeLoggedPlayer (Authentication authentication) {
        System.out.println("generacion del JSON completo");
        Map<String, Object> dto = new LinkedHashMap<>();
        if(authentication == null || authentication instanceof AnonymousAuthenticationToken)
            dto.put("player", "Guest");
        else
            dto.put("player", loggedPlayerDTO(playerRepository.findByUserName(authentication.getName())));

        System.out.println("llama a getGames");
        dto.put("games",getGames());
        return dto;
    }

     public Map<String, Object> loggedPlayerDTO(Player player){
        Map<String, Object>dto = new LinkedHashMap<>();
        dto.put("id", player.getId());
        dto.put("username", player.getUserName());
        dto.put("gpid", player.getId());
        return dto;
     }

    @RequestMapping(path = "/players", method = RequestMethod.POST)
    public ResponseEntity<String> createUser(@RequestParam String username ,@RequestParam String password) {
        System.out.println("username:" + username);
        System.out.println("password:" + password);
        if (username.isEmpty()) {
            return new ResponseEntity<>("No name given", HttpStatus.FORBIDDEN);
        }

        Player player = playerRepository.findByUserName(username);
        if (player != null) {
            return new ResponseEntity<>("Name already used", HttpStatus.CONFLICT);
        }

        Player addedPlayer = new Player(username , passwordEncoder().encode(password));
        System.out.println("addedplayer: " + addedPlayer);
        playerRepository.save(addedPlayer);

        return new ResponseEntity<>("Named added", HttpStatus.CREATED);
    }
    private Map<String, Object> makeMap(String key, Object value) {
        Map<String, Object> map = new HashMap<>();
        map.put(key, value);
        return map;
    }

    @RequestMapping(path = "/games", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> createGame( Authentication authentication) {
        Player player = getLoggedPlayer(authentication);
        GamePlayer gamePlayer = gamePlayerRepository.findById(player.getId()).orElse(null);
        if (gamePlayer == null) {
            return new ResponseEntity<>(makeMap("error", "You need to be logged in"), HttpStatus.UNAUTHORIZED);
        }
        Game newGame = gameRepository.save(new Game(new Date()));

        GamePlayer newGamePlayer = gamePlayerRepository.save(new GamePlayer(newGame, player));

        return new ResponseEntity<>(makeMap("id", newGamePlayer.getId()), HttpStatus.CREATED);
    }
    @RequestMapping(path="games/players/{gamePlayerId}/ships", method = RequestMethod.POST)
       public ResponseEntity<Map<String, Object>> addShips(@PathVariable long gamePlayerId, @RequestBody Set<Ship> ships, Authentication authentication) {

           if(isGuest(authentication)) return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

           Player player = getLoggedPlayer(authentication);

           GamePlayer gamePlayer = gamePlayerRepository.findById(gamePlayerId).orElse(null);

           if(gamePlayer == null) return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

           if(player.getId() != gamePlayer.getPlayer().getId()) return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);



           if(gamePlayer.getShips().size() >= 5) return new ResponseEntity<>( HttpStatus.FORBIDDEN);

           for (Ship ship : ships){
               ship.setGamePlayer(gamePlayer);
               shipRepository.save(ship);
           }

           return new ResponseEntity<>( makeMap("OK", "Ships placed"), HttpStatus.OK);

       }
    private boolean isGuest(Authentication authentication) {
        return authentication == null || authentication instanceof AnonymousAuthenticationToken;
    }

    @RequestMapping(path="/games/players/{gamePlayerId}/salvoes", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> addSalvoes(@PathVariable long gamePlayerId, @RequestBody Salvo salvo, Authentication authentication) {

        if(isGuest(authentication)) return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        Player player = getLoggedPlayer(authentication);

        GamePlayer gamePlayer = gamePlayerRepository.findById(gamePlayerId).orElse(null);

        if(gamePlayer == null) return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        if(player.getId() != gamePlayer.getPlayer().getId()) return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        //if(salvo.getTurno() < 3) return new ResponseEntity<>( HttpStatus.FORBIDDEN);

        salvo.setGamePlayer(gamePlayer);
        salvoRepository.save(salvo);

        if (!gamePlayer.isTurnoLoaded(salvo.getTurno())) {
            salvo.setGamePlayer(gamePlayer);
            salvoRepository.save(salvo);

        return new ResponseEntity<>( makeMap("OK", "Salvo created"), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(makeMap("error", "Player already placed salvoes in this turn"), HttpStatus.FORBIDDEN);
        }

        }

    private Map<String, Object> makeHitsDTO(GamePlayer selfGP, GamePlayer opponentGP){

        Map<String, Object> dto = new LinkedHashMap<>();

        dto.put("self", getHits(selfGP, opponentGP));

        dto.put("opponent", getHits(opponentGP, selfGP));

        return dto;

    }
    private List<Map> getHits(GamePlayer gamePlayer, GamePlayer opponentGameplayer) {
        List<Map> hits = new ArrayList<>();

        Integer carrierDamage = 0;
        Integer battleshipDamage = 0;
        Integer submarineDamage = 0;
        Integer destroyerDamage = 0;
        Integer patrolboatDamage = 0;

        List <String> carrierLocation = new ArrayList<>();
        List <String> battleshipLocation = new ArrayList<>();
        List <String> submarineLocation = new ArrayList<>();
        List <String> destroyerLocation = new ArrayList<>();
        List <String> patrolboatLocation = new ArrayList<>();

        gamePlayer.getShips().forEach(ship -> {
            switch (ship.getShipType()) {
                case "carrier":
                    carrierLocation.addAll(ship.getLocations());
                    break;
                case "battleship":
                    battleshipLocation.addAll(ship.getLocations());
                    break;
                case "submarine":
                    submarineLocation.addAll(ship.getLocations());
                    break;
                case "destroyer":
                    destroyerLocation.addAll(ship.getLocations());
                    break;
                case "patrolboat":
                    patrolboatLocation.addAll(ship.getLocations());
                    break;
            }
        });

        for (Salvo salvo : opponentGameplayer.getSalvoes()) {
            Integer carrierHitsInTurn = 0;
            Integer battleshipHitsInTurn = 0;
            Integer submarineHitsInTurn = 0;
            Integer destroyerHitsInTurn = 0;
            Integer patrolboatHitsInTurn = 0;
            Integer missedShots = salvo.getLocations().size();

            Map<String, Object> hitsMapPerTurn = new LinkedHashMap<>();
            Map<String, Object> damagesPerTurn = new LinkedHashMap<>();

            List<String> salvoLocationsList = new ArrayList<>();
            List<String> hitCellsList = new ArrayList<>();
            salvoLocationsList.addAll(salvo.getLocations());
            for (String salvoShot : salvoLocationsList) {
                if (carrierLocation.contains(salvoShot)) {
                    carrierDamage++;
                    carrierHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
                if (battleshipLocation.contains(salvoShot)) {
                    battleshipDamage++;
                    battleshipHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
                if (submarineLocation.contains(salvoShot)) {
                    submarineDamage++;
                    submarineHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
                if (destroyerLocation.contains(salvoShot)) {
                    destroyerDamage++;
                    destroyerHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
                if (patrolboatLocation.contains(salvoShot)) {
                    patrolboatDamage++;
                    patrolboatHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
            }

            damagesPerTurn.put("carrierHits", carrierHitsInTurn);
            damagesPerTurn.put("battleshipHits", battleshipHitsInTurn);
            damagesPerTurn.put("submarineHits", submarineHitsInTurn);
            damagesPerTurn.put("destroyerHits", destroyerHitsInTurn);
            damagesPerTurn.put("patrolboatHits", patrolboatHitsInTurn);
            damagesPerTurn.put("carrier", carrierDamage);
            damagesPerTurn.put("battleship", battleshipDamage);
            damagesPerTurn.put("submarine", submarineDamage);
            damagesPerTurn.put("destroyer", destroyerDamage);
            damagesPerTurn.put("patrolboat", patrolboatDamage);

            hitsMapPerTurn.put("turn", salvo.getTurno());
            hitsMapPerTurn.put("hitLocations", hitCellsList);
            hitsMapPerTurn.put("damages", damagesPerTurn);
            hitsMapPerTurn.put("missed", missedShots);
            hits.add(hitsMapPerTurn);
        }

        return hits;

    }

//holis:)

}


