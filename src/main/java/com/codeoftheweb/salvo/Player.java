package com.codeoftheweb.salvo;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Player {

    public String getUserName() {
        return userName;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;

    @OneToMany(mappedBy = "player",fetch =FetchType.EAGER)
    private Set<Score> scores;

    @OneToMany(mappedBy ="player",fetch = FetchType.EAGER)
    private Set<GamePlayer> gamePlayers;

    private String userName;

    private String password;

    public Player(){}

    public Player(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public Player(String userName) {
            this.userName = userName;
    }


    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void addGamePlayer(GamePlayer gamePlayer){
        gamePlayer.setGamePlayer(this);
        gamePlayer.add(gamePlayer);
    }

    public String toString() {
        return userName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getWins(Set<Score> scores){
        return scores
        .stream()
        .filter(score -> score.getScore() == 1.0)
        .count();
        }
    public long getLoses(Set<Score> scores){
        return scores
        .stream()
        .filter(score -> score.getScore() == 0.5)
        .count();
        }
    public long getDraws(Set<Score> scores){
        return scores
        .stream()
        .filter(score -> score.getScore() == 0.0)
        .count();
        }

    public Set<Score> getScores() {
        return scores;
    }

    public void setScores(Set<Score> scores) {
        this.scores = scores;
    }

    public double getScore(Player player){
        return getWins(player.getScores())*1 + getDraws(player.getScores())*0.5;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}


