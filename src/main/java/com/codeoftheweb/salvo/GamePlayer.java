package com.codeoftheweb.salvo;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.Set;

@Entity
public class GamePlayer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;

    @OneToMany(mappedBy = "gamePlayer", fetch = FetchType.EAGER)
    private Set<Salvo> salvoes;

    @OneToMany(mappedBy = "gamePlayer", fetch = FetchType.EAGER)
    private Set<Ship> ships;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "player_id")
    private Player player;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "game_id")
    private Game game;

    private Date joinDate;

    public GamePlayer() {
    }

    public GamePlayer(Game game, Player player) {
        this.setGame(game);
        this.setPlayer(player);
        this.joinDate = new Date();
    }
    //id
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    //player

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    //game
    @JsonIgnore
    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    //join date
    public Date getJoinDate() {
        return joinDate;
    }
    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    //gameplayer
    public void setGamePlayer(Player player) {
    }
    public void add(GamePlayer gamePlayer) {
    }

    //ships
    public void setShips(Set<Ship> ships) {
        this.ships = ships;
    }
    public Set<Ship> getShips() {
        return ships;
    }

    //salvoes
    public Set<Salvo> getSalvoes() {
        return salvoes;
    }

    public void setSalvoes(Set<Salvo> salvoes) {
        this.salvoes = salvoes;
    }


    public boolean isTurnoLoaded (int turn) {
        Salvo s = this.getSalvoes()
                .stream()
                .filter(salvo -> salvo.getTurno() == turn)
                .findFirst()
                .orElse(null);

        if (s == null) {
            return false;
        } else {
            return true;
        }
    }


}
