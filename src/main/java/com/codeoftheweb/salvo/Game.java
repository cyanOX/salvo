package com.codeoftheweb.salvo;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;

    @OneToMany(mappedBy = "game",fetch =FetchType.EAGER)
    private
    Set<Score> score;

    @OneToMany(mappedBy = "game",fetch =FetchType.EAGER)
    Set<GamePlayer> gamePlayers;

    private Date creationDate;

    public Game() { }

    public Game(Date creationDate) {
        this.creationDate = creationDate;
    }

    public void setCreationDate(Date creationDate) {

        this.creationDate = creationDate;
    }
    public void addGamePlayers(GamePlayer gamePlayer){
        gamePlayer.setGame(this);
        gamePlayers.add(gamePlayer);
    }

    public Date getCreationDate() { return creationDate;}

    public Long getId() {
    return id;
    }

    public Set<GamePlayer> getGamePlayers() {
    return gamePlayers;
    }


    public Set<Score> getScore() {
        return (Set<Score>) score;
    }

    public void setScore(Set<Score> score) {
        this.score = score;
    }
}

