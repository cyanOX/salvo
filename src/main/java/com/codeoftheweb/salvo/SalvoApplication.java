package com.codeoftheweb.salvo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootApplication
public class SalvoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SalvoApplication.class, args);
	}

	@Bean
	public PasswordEncoder passwordEncoder(){
		return (PasswordEncoder) PasswordEncoderFactories.createDelegatingPasswordEncoder();
	}

	@Bean
	public CommandLineRunner initData(PlayerRepository playerRepository, GameRepository gameRepository, GamePlayerRepository gamePlayerRepository, ShipRepository shipRepository, SalvoRepository salvoRepository, ScoreRepository scoreRepository) {
		return (args) -> {

			Player player1 = new Player("oriana@gmail.com" , passwordEncoder().encode("24"));
			Player player2 = new Player("valeria@gmail.com", passwordEncoder().encode("56"));
			Player player3 = new Player("ornella@gmail.com", passwordEncoder().encode("78"));

			playerRepository.save(player1);
			playerRepository.save(player2);
			playerRepository.save(player3);

			Date fecha1 = new Date();
			Game game1 = new Game(fecha1);
			Date fecha2 = fecha1.from(fecha1.toInstant().plusSeconds(3600));
			Game game2 = new Game(fecha2);
			Date fecha3 = fecha2.from(fecha2.toInstant().plusSeconds(3600));
			Game game3 = new Game(fecha3);
			Date fecha4 = fecha3.from(fecha3.toInstant().plusSeconds(3600));

			gameRepository.save(game1);
			gameRepository.save(game2);
			gameRepository.save(game3);

			GamePlayer gamePlayer1 = new GamePlayer(game1, player1);
			GamePlayer gamePlayer2 = new GamePlayer(game1, player2);
			GamePlayer gamePlayer3 = new GamePlayer(game2, player2);
			GamePlayer gamePlayer4 = new GamePlayer(game2, player3);
			GamePlayer gamePlayer5 = new GamePlayer(game3, player1);
			GamePlayer gamePlayer6 = new GamePlayer(game3, player3);

			gamePlayerRepository.save(gamePlayer1);
			gamePlayerRepository.save(gamePlayer2);
			gamePlayerRepository.save(gamePlayer3);
			gamePlayerRepository.save(gamePlayer4);
			gamePlayerRepository.save(gamePlayer5);
			gamePlayerRepository.save(gamePlayer6);

			Score score1 = new Score(game1, player1, 1, fecha2);
			Score score2 = new Score(game1, player2, 0.5, fecha2);
			Score score3 = new Score(game2, player2, 0, fecha3);
			Score score4 = new Score(game2, player3, 1, fecha3);
			Score score5 = new Score(game3, player1, 0.5, fecha4);
			Score score6 = new Score(game3, player3, 1, fecha4);

			scoreRepository.save(score1);
			scoreRepository.save(score2);
			scoreRepository.save(score3);
			scoreRepository.save(score4);
			scoreRepository.save(score5);
			scoreRepository.save(score6);

			//shiplocations
			List<String> locations1 = new ArrayList<>();
			locations1.add("H1");
			locations1.add("H2");
			locations1.add("H3");

			List<String> locations2 = new ArrayList<>();
			locations2.add("F5");
			locations2.add("F6");
			locations2.add("F7");

			List<String> locations3 = new ArrayList<>();
			locations3.add("J2");
			locations3.add("J3");

			List<String> locations4 = new ArrayList<>();
			locations4.add("B3");
			locations4.add("C3");
			locations4.add("D3");
			locations4.add("E3");

			List<String> locations5 = new ArrayList<>();
			locations5.add("E3");
			locations5.add("E4");
			locations5.add("E5");
			locations5.add("E6");
			locations5.add("E7");

			List<String> locations6 = new ArrayList<>();
			locations6.add("D8");
			locations6.add("D9");

			//salvolocations
			//JUEGO 1 TURNO1
			List<String> salvoLocations1 = new ArrayList<>();
			salvoLocations1.add("F7");
			salvoLocations1.add("E5");

			List<String> salvoLocations2 = new ArrayList<>();
			salvoLocations2.add("J2");

			//JUEGO1 TURNO2
			List<String> salvoLocations3 = new ArrayList<>();
			salvoLocations3.add("A2");

			List<String> salvoLocations4 = new ArrayList<>();
			salvoLocations4.add("H2");
			salvoLocations4.add("H3");
			salvoLocations4.add("H4");

			//JUEGO2 TURNO1
			List<String> salvoLocations5 = new ArrayList<>();
			salvoLocations5.add("D3");
			salvoLocations5.add("D4");

			List<String> salvoLocations6 = new ArrayList<>();
			salvoLocations6.add("J3");
			salvoLocations6.add("J4");

			//JUEGO2 TURNO2
			List<String> salvoLocations7 = new ArrayList<>();
			salvoLocations7.add("D2");

			List<String> salvoLocations8 = new ArrayList<>();
			salvoLocations8.add("J2");
			salvoLocations8.add("B3");

			//JUEGO3 TURNO1
			List<String> salvoLocations9 = new ArrayList<>();
			salvoLocations9.add("A8");

			List<String> salvoLocations10 = new ArrayList<>();
			salvoLocations10.add("E5");
			salvoLocations10.add("D5");

			//JUEGO3 TURNO2
			List<String> salvoLocations11 = new ArrayList<>();
			salvoLocations11.add("D8");
			salvoLocations11.add("D7");

			List<String> salvoLocations12 = new ArrayList<>();
			salvoLocations12.add("E6");
			salvoLocations12.add("E7");
			salvoLocations12.add("E8");

			//ship
			Ship ship1 = new Ship("Destroyer", gamePlayerRepository.getOne((long) 1), locations1);
			Ship ship2 = new Ship("Submarine", gamePlayerRepository.getOne((long) 2), locations2);
			Ship ship3 = new Ship("Patrol", gamePlayerRepository.getOne((long) 3), locations3);
			Ship ship4 = new Ship("Battleship", gamePlayerRepository.getOne((long) 4), locations4);
			Ship ship5 = new Ship("Carrier", gamePlayerRepository.getOne((long) 5), locations5);
			Ship ship6 = new Ship("Patrol", gamePlayerRepository.getOne((long) 6), locations6);

			shipRepository.save(ship1);
			shipRepository.save(ship2);
			shipRepository.save(ship3);
			shipRepository.save(ship4);
			shipRepository.save(ship5);
			shipRepository.save(ship6);

			//salvo
			Salvo salvo1 = new Salvo(1, gamePlayerRepository.getOne((long) 1), salvoLocations1);
			Salvo salvo2 = new Salvo(1, gamePlayerRepository.getOne((long) 2), salvoLocations2);
			Salvo salvo3 = new Salvo(2, gamePlayerRepository.getOne((long) 1), salvoLocations3);
			Salvo salvo4 = new Salvo(2, gamePlayerRepository.getOne((long) 2), salvoLocations4);
			Salvo salvo5 = new Salvo(1, gamePlayerRepository.getOne((long) 3), salvoLocations5);
			Salvo salvo6 = new Salvo(1, gamePlayerRepository.getOne((long) 4), salvoLocations6);
			Salvo salvo7 = new Salvo(2, gamePlayerRepository.getOne((long) 3), salvoLocations7);
			Salvo salvo8 = new Salvo(2, gamePlayerRepository.getOne((long) 4), salvoLocations8);
			Salvo salvo9 = new Salvo(1, gamePlayerRepository.getOne((long) 5), salvoLocations9);
			Salvo salvo10 = new Salvo(1, gamePlayerRepository.getOne((long) 6), salvoLocations10);
			Salvo salvo11 = new Salvo(2, gamePlayerRepository.getOne((long) 5), salvoLocations11);
			Salvo salvo12 = new Salvo(2, gamePlayerRepository.getOne((long) 6), salvoLocations12);

			salvoRepository.save(salvo1);
			salvoRepository.save(salvo2);
			salvoRepository.save(salvo3);
			salvoRepository.save(salvo4);
			salvoRepository.save(salvo5);
			salvoRepository.save(salvo6);
			salvoRepository.save(salvo7);
			salvoRepository.save(salvo8);
			salvoRepository.save(salvo9);
			salvoRepository.save(salvo10);
			salvoRepository.save(salvo11);
			salvoRepository.save(salvo12);

		};
	}

	@Configuration
	class WebSecurityConfiguration extends GlobalAuthenticationConfigurerAdapter {

		@Autowired
		PlayerRepository playerRepository;

		@Override
		public void init(AuthenticationManagerBuilder auth) throws Exception {
			auth.userDetailsService(inputName -> {
				Player player = playerRepository.findByUserName(inputName);
				if (player != null) {
					return new User(player.getUserName(), player.getPassword(),
							AuthorityUtils.createAuthorityList("USER"));
				} else {
					throw new UsernameNotFoundException("Unknown user: " + inputName);
				}
			});
		}
	}

	@EnableWebSecurity
	@Configuration
	class WebSecurityConfig extends WebSecurityConfigurerAdapter {

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.authorizeRequests()
					.antMatchers("/web/games_3.html").permitAll()
					.antMatchers("/web/**").permitAll()
					.antMatchers("/api/games").permitAll()
					.antMatchers("/api/players").permitAll()
					.antMatchers("/api/leaderBoard").permitAll()
					.antMatchers("/api/game_view/*").hasAuthority("USER")
					.antMatchers("/rest/*").denyAll()
					.anyRequest().permitAll();


			http.formLogin()
					.usernameParameter("username")
					.passwordParameter("password")
					.loginPage("/api/login");

			http.logout()
					.logoutUrl("/api/logout");

			// turn off checking for CSRF tokens
			http.csrf().disable();

			// if user is not authenticated, just send an authentication failure response
			http.exceptionHandling().authenticationEntryPoint((req, res, exc) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

			// if login is successful, just clear the flags asking for authentication
			http.formLogin().successHandler((req, res, auth) -> clearAuthenticationAttributes(req));

			// if login fails, just send an authentication failure response
			http.formLogin().failureHandler((req, res, exc) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

			// if logout is successful, just send a success response
			http.logout().logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler());


			http.authorizeRequests()
					.anyRequest()
					.fullyAuthenticated()
					.and()
					.httpBasic();
		}
			private void clearAuthenticationAttributes(HttpServletRequest request){
				HttpSession session = request.getSession(false);
				if (session != null) {
					session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);

				}

			}
		}
	}

