package com.codeoftheweb.salvo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
public class Score {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "player_id")
    private Player player;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "game_id")
    private Game game;

    private Date finishDate;

    private double score;

    public Score() { }

    public Score(Date finishDate) {
        this.finishDate = finishDate;
    }
    public Score (Game game, Player player, double score, Date finishDate) {
        this.setGame(game);
        this.setPlayer(player);
        this.setScore(score);
        this.setFinishDate(finishDate);
    }


    //id
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    //player
    @JsonIgnore
    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    //game
    @JsonIgnore
    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    //finish date
    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    //double score
    public double getScore() { return score; }

    public void setScore(double score) {this.score = score;}



}